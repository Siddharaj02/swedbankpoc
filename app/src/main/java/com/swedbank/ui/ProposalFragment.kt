package com.swedbank.ui

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.os.bundleOf
import androidx.core.view.marginTop
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

import com.swedbank.R
import com.swedbank.ui.dashboard.DashboardContent
import com.swedbank.ui.landing.LandingActivity


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class ProposalFragment : Fragment() {


    private var param1: String? = null
    private var param2: String? = null

    private var listener: OnPlanProposalListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        val view = inflater.inflate(R.layout.fragment_plan_proposal, container, false)

        val continueButton = view.findViewById<Button>(R.id.btnSaveProposalId)
        continueButton.setOnClickListener{view ->
            listener?.onPlanProposal(view)
        }



        return view
    }

    fun onButtonPressed(view: View) {
        listener?.onPlanProposal(view)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnPlanProposalListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnPlanProposalListener {
        fun onPlanProposal(view: View)
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar!!.title = resources.getString(R.string.plan_proposal)
    }
}