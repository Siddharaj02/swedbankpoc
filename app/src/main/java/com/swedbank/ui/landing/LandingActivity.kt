package com.swedbank.ui.landing

import android.net.Uri
import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import android.view.MenuItem
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.navigation.Navigation.createNavigateOnClickListener
import com.swedbank.R
import com.swedbank.ui.*
import com.swedbank.ui.dashboard.DashboardFragment

class LandingActivity : AppCompatActivity(),
    NavigationView.OnNavigationItemSelectedListener,
    LandingFragment.OnFragmentListener,
    DashboardFragment.OnListFragmentListener,
    NewInsuranceVillaHomeFragment.OnNewInsuranceListener,
    BasicInfoFragment.OnBasicInfoListener,
    BuildingInfoFragment.OnBuildingInfoListener,
    OtherInfoFragment.OnOtherInfoListener,
    ProposalFragment.OnPlanProposalListener{


    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented")
    }

    override fun onOnNewInsuranceVillaHomeInteraction(view: View) {
        view.setOnClickListener(createNavigateOnClickListener(R.id.BasicInfoFragment))
    }

    override fun onBasicInfo(view: View) {
        view.setOnClickListener(createNavigateOnClickListener(R.id.BuildingInfoFragment))
    }

    override fun onBuildingInfo(view: View) {
        view.setOnClickListener(createNavigateOnClickListener(R.id.OtherInfoFragment))
    }

    override fun onOtherInfo(view: View) {
        view.setOnClickListener(createNavigateOnClickListener(R.id.ProposalFragment))
    }

    override fun onPlanProposal(view: View) {
        view.setOnClickListener(createNavigateOnClickListener(R.id.OverviewInsuranceFragment))
    }




    override fun onListFragmentInteraction(dashboardText: String, view: View) {
        if (dashboardText != "Villa Home") {
            Toast.makeText(this, dashboardText, Toast.LENGTH_SHORT).show()
        } else {
            view.setOnClickListener(createNavigateOnClickListener(R.id.NewInsuranceVillaHomeFragment))
        }
    }

    private var menuM: Menu? = null
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuM = menu
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.inflateMenu(R.menu.toolbar_menu)
        toolbar.setOnMenuItemClickListener { item: MenuItem? ->
            onOptionsItemSelected(item)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            R.id.navigateNext -> {
                // val view : View =  item.getas View
                //  view.setOnClickListener(createNavigateOnClickListener(R.id.BasicInfoFragment))
            }
        }

        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        //supportActionBar?.setDisplayShowTitleEnabled(true)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return Navigation.findNavController(this, R.id.nav_host_fragment).navigateUp()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.menu_home -> {
                Toast.makeText(
                    applicationContext,
                    resources.getString(R.string.in_progres),
                    Toast.LENGTH_SHORT
                ).show()
            }
            R.id.menu_economical_overview -> {

            }
            R.id.menu_pay_and_transfer -> {

            }
            R.id.menu_save_place -> {

            }
            R.id.menu_borrow -> {

            }
            R.id.menu_ensure -> {

            }

            R.id.menu_options -> {

            }

            R.id.menu_contact_us -> {

            }

            R.id.menu_help -> {

            }

            R.id.menu_logout -> {

            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }


}
