package com.swedbank.ui.landing

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController

import com.swedbank.R

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class LandingFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    private var listener: OnFragmentListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_landing, container, false)

        val cardAccommodation = view.findViewById<CardView>(R.id.card_accommodation)
        val cardAutomobile = view.findViewById<CardView>(R.id.card_automobile)

        var bundleAccommodation = bundleOf("dashboardItem" to 1)
        var bundleAutomobile = bundleOf("dashboardItem" to 2)
        cardAccommodation.setOnClickListener { view ->
            view.findNavController().navigate(R.id.dashboardFragment, bundleAccommodation)
        }
        cardAutomobile.setOnClickListener { view ->
            view.findNavController().navigate(R.id.dashboardFragment, bundleAutomobile)
        }
        return view
    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentListener {
        fun onFragmentInteraction(uri: Uri)
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar!!.title = resources.getString(R.string.app_name)
    }
}
