package com.swedbank.ui

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.opengl.Visibility
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.createNavigateOnClickListener
import com.swedbank.R
import com.swedbank.ui.landing.LandingActivity
import kotlinx.android.synthetic.main.fragment_new_insurance_villa_home.*


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class NewInsuranceVillaHomeFragment : Fragment(), AdapterView.OnItemSelectedListener {


    private var param1: String? = null
    private var param2: String? = null

    private var imgStepperOne: ImageView? = null
    private var imgStepperTwo: ImageView? = null
    private var imgStepperThree: ImageView? = null
    private var imgStepperFour: ImageView? = null
    private var imgInformation: ImageView? = null

    private var txtStepperOne: TextView? = null
    private var txtStepperTwo: TextView? = null
    private var txtStepperThree: TextView? = null
    private var txtStepperFour: TextView? = null
    private var textStep: TextView? = null

    private var listener: OnNewInsuranceListener? = null
    private var selectMunicipality: Spinner? = null
    private var selectPreviousCompany: Spinner? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, p3: Long) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        when (position) {
            0 -> {
                selectMunicipality?.setSelection(position)
                Toast.makeText(
                    context,
                    getString(R.string.select_municipality) + " " + selectMunicipality?.getChildAt(position).toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }

            1 -> {
                selectPreviousCompany?.setSelection(position)
                Toast.makeText(
                    context,
                    getString(R.string.select_previous_company) + " " + selectPreviousCompany?.getChildAt(position).toString(),
                    Toast.LENGTH_SHORT
                ).show()

            }
        }
    }


    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_new_insurance_villa_home, container, false)
        setHasOptionsMenu(true);
        /* val toolbar: Toolbar = view.findViewById(R.id.toolbar)
         (activity as LandingActivity).setSupportActionBar(toolbar)
         (activity as LandingActivity).supportActionBar?.setDisplayShowTitleEnabled(true)
 */

        imgStepperOne = view.findViewById<ImageView>(R.id.stepperImageOneId)
        imgStepperTwo = view.findViewById<ImageView>(R.id.stepperImageTwoId)
        imgStepperThree = view.findViewById<ImageView>(R.id.stepperImageThreeId)
        imgStepperFour = view.findViewById<ImageView>(R.id.stepperImageFourId)
        imgInformation = view.findViewById<ImageView>(R.id.imgInformationId)

        txtStepperOne = view.findViewById<TextView>(R.id.txtStepperOneId)
        txtStepperTwo = view.findViewById<TextView>(R.id.txtStepperTwoId)
        txtStepperThree = view.findViewById<TextView>(R.id.txtStepperThreeId)
        txtStepperFour = view.findViewById<TextView>(R.id.txtStepperFourId)
        textStep = view.findViewById<TextView>(R.id.textStepId)

        imgInformation?.visibility = View.GONE
        txtStepperOne?.setTypeface(null, Typeface.BOLD)
        textStep?.text = "Step 1 : Insurance site and customer information"

        selectMunicipality = view.findViewById<Spinner>(R.id.select_municipality)
        selectMunicipality?.onItemSelectedListener
        selectMunicipality?.isSelected = false

        selectPreviousCompany = view.findViewById<Spinner>(R.id.select_previous_company)
        selectPreviousCompany?.isSelected = false
        selectPreviousCompany?.onItemSelectedListener


        val continueButton = view.findViewById<Button>(R.id.continueButton)

        val municipalityNames = arrayOf(resources.getStringArray(R.array.select_municipality_array))
        val previousCompanyNames = arrayOf(resources.getStringArray(R.array.select_municipality_array))

        //selectMunicipality.adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, municipalityNames)
        // selectPreviousCompany.adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, previousCompanyNames)

        continueButton.setOnClickListener(createNavigateOnClickListener(R.id.BasicInfoFragment))

        /*selectMunicipality.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                Toast.makeText(
                    context,
                    getString(R.string.select_municipality) + " " + selectMunicipality.selectedItem.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        selectPreviousCompany.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                Toast.makeText(
                    context,
                    getString(R.string.select_municipality) + " " + selectPreviousCompany.selectedItem.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }*/

        return view
    }

    fun onButtonPressed(view: View) {
        listener?.onOnNewInsuranceVillaHomeInteraction(view)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnNewInsuranceListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnNewInsuranceListener {
        fun onOnNewInsuranceVillaHomeInteraction(view: View)
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar!!.title = resources.getString(R.string.new_villa_insurance)
    }


    /*  private var menuM: Menu? = null
      override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
          //super.onCreateOptionsMenu(menu, inflater)
          // Inflate the menu;     this adds items to the action bar if it is present.
          menu?.clear()
          menuM = menu
          inflater?.inflate(R.menu.toolbar_menu, menu)
      }

      override fun onOptionsItemSelected(item: MenuItem): Boolean {
          when (item.itemId) {
              R.id.navigateNext -> {
                  val view = menuM?.findItem(R.id.navigateNext)?.actionView!!
                  listener?.onOnNewInsuranceVillaHomeInteraction(view)
              }
          }
          return true
      }*/
}
