package com.swedbank.ui

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.os.bundleOf
import androidx.core.view.marginTop
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

import com.swedbank.R
import com.swedbank.ui.dashboard.DashboardContent
import com.swedbank.ui.landing.LandingActivity
import android.R.attr.gravity
import android.app.Dialog
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.view.*
import android.widget.LinearLayout
import androidx.annotation.ColorInt
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import com.swedbank.ui.dialog.InformationDialog
import kotlinx.android.synthetic.main.add_another_view.*


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class BasicInfoFragment : Fragment() {


    private var imgStepperOne: ImageView? = null
    private var imgStepperTwo: ImageView? = null
    private var imgStepperThree: ImageView? = null
    private var imgStepperFour: ImageView? = null
    private var imgInformation: ImageView? = null

    private var txtStepperOne: TextView? = null
    private var txtStepperTwo: TextView? = null
    private var txtStepperThree: TextView? = null
    private var txtStepperFour: TextView? = null
    private var textStep: TextView? = null
    private var infoCard: CardView? = null


    private var param1: String? = null
    private var param2: String? = null

    private var listener: OnBasicInfoListener? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        val view = inflater.inflate(R.layout.fragment_basic_info, container, false)


        imgStepperOne = view.findViewById<ImageView>(R.id.stepperImageOneId)
        imgStepperTwo = view.findViewById<ImageView>(R.id.stepperImageTwoId)
        imgStepperThree = view.findViewById<ImageView>(R.id.stepperImageThreeId)
        imgStepperFour = view.findViewById<ImageView>(R.id.stepperImageFourId)
        imgInformation = view.findViewById<ImageView>(R.id.imgInformationId)

        txtStepperOne = view.findViewById<TextView>(R.id.txtStepperOneId)
        txtStepperTwo = view.findViewById<TextView>(R.id.txtStepperTwoId)
        txtStepperThree = view.findViewById<TextView>(R.id.txtStepperThreeId)
        txtStepperFour = view.findViewById<TextView>(R.id.txtStepperFourId)
        textStep = view.findViewById<TextView>(R.id.textStepId)


        imgStepperOne?.setImageResource(R.drawable.check_circle_outline)
        imgStepperTwo?.setImageResource(R.drawable.checkbox_blank_circle)

        txtStepperTwo?.setTypeface(null, Typeface.BOLD)
        textStep?.text = "Step 2 : People in the household"

        imgInformation?.setOnClickListener {

            InformationDialog().customDialog(requireActivity(),resources.getString(R.string.basic_information))
        }

        val addButton = view.findViewById<ImageButton>(R.id.img_btn_add_person)
        val linearLayout = view.findViewById<LinearLayout>(R.id.linearLayout)
        val continueButton = view.findViewById<Button>(R.id.continueButton)
        continueButton.setOnClickListener { view ->
            listener?.onBasicInfo(view)
        }

        var personList: List<String>
        addButton.setOnClickListener { view ->
            // Get the LayoutInflater from Context
            val layoutInflater: LayoutInflater = LayoutInflater.from(context)
            // Inflate the layout using LayoutInflater
            val view: View = layoutInflater.inflate(
                R.layout.add_another_view, // Custom view/ layout
                linearLayout, // Root layout to attach the view
                false // Attach with root layout or not
            )

            val imgRemove = view.findViewById<ImageView>(R.id.imgReomveId)
            val subLayout = view.findViewById<ConstraintLayout>(R.id.clParent)
            val etAddPerson = view.findViewById<TextInputEditText>(R.id.etAddPersonId)

            imgRemove.setOnClickListener {
                    linearLayout.removeView(subLayout)
            }

            linearLayout.addView(view)
            personList = listOf(etAddPerson.text.toString())
        }


        return view
    }

    fun onButtonPressed(view: View) {
        listener?.onBasicInfo(view)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnBasicInfoListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnBasicInfoListener {
        fun onBasicInfo(view: View)
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar!!.title = resources.getString(R.string.basic_info)
    }
}