package com.swedbank.ui.dashboard

import com.swedbank.R
import java.util.ArrayList


object DashboardContent {

    var ACCOMMODATION_MAP: Map<Int, String>
    val ACCOMMODATION_LIST: MutableList<Int> = ArrayList()

    var AUTOMOBILE_MAP: Map<Int, String>
    val AUTOMOBILE_LIST: MutableList<Int> = ArrayList()

    var DASHBOARD_MAP: Map<Int, String>
    val DASHBOARD_LIST: MutableList<Int> = ArrayList()

    init {
        ACCOMMODATION_MAP = mapOf(
            R.drawable.ic_account_balance to "Villa Home",
            R.drawable.ic_home to "Home",
            R.drawable.ic_assignment to "Villa(property only)",
            R.drawable.ic_description to "Residence (not home insurance)",
            R.drawable.ic_filter_frames to "Holiday home",
            R.drawable.ic_villa to "Extended travel insurance"
        )
        for (key in ACCOMMODATION_MAP.keys) {
            addAccommodationItem(key)
        }


        AUTOMOBILE_MAP = mapOf(
            R.drawable.ic_car to "Car",
            R.drawable.ic_mobile to "Engine",
            R.drawable.ic_contact to "Caravan",
            R.drawable.ic_engine to "Trailer",
            R.drawable.ic_filter_frames to "Snowmobile"
        )
        for (key in AUTOMOBILE_MAP.keys) {
            addAutomobileItem(key)
        }

        DASHBOARD_MAP = mapOf(
            1 to "Accommodation",
            2 to "Automobile"
        )
        for (key in DASHBOARD_MAP.keys) {
            addDashboardItem(key)
        }
    }

    private fun addAccommodationItem(item: Int) {
        ACCOMMODATION_LIST.add(item)
    }

    private fun addAutomobileItem(item: Int) {
        AUTOMOBILE_LIST.add(item)
    }

    private fun addDashboardItem(item: Int) {
        DASHBOARD_LIST.add(item)
    }
}
