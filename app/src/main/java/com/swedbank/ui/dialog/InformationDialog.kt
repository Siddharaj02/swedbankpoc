package com.swedbank.ui.dialog

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.swedbank.R

class  InformationDialog {

  fun customDialog(context: Activity,informationText:String){
      var infodialog  = Dialog(context)
      // infodialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
      infodialog.setContentView(R.layout.cd_information)
      infodialog.setTitle("Information")
      val infoText = infodialog.findViewById<TextView>(R.id.txtInfoId)
      infoText.text = informationText

      //  infodialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
      infodialog.show()
  }

}