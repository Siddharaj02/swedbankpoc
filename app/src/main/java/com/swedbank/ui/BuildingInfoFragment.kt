package com.swedbank.ui

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.os.bundleOf
import androidx.core.view.marginTop
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

import com.swedbank.R
import com.swedbank.ui.dashboard.DashboardContent
import com.swedbank.ui.dialog.InformationDialog
import com.swedbank.ui.landing.LandingActivity


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class BuildingInfoFragment : Fragment() {

    private var imgStepperOne: ImageView? = null
    private var imgStepperTwo: ImageView? = null
    private var imgStepperThree: ImageView? = null
    private var imgStepperFour: ImageView? = null
    private var imgInformation: ImageView? = null

    private var txtStepperOne: TextView? = null
    private var txtStepperTwo: TextView? = null
    private var txtStepperThree: TextView? = null
    private var txtStepperFour: TextView? = null
    private var textStep: TextView? = null

    private var param1: String? = null
    private var param2: String? = null

    private var listener: OnBuildingInfoListener? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        val view = inflater.inflate(R.layout.fragment_building_info, container, false)


        imgStepperOne = view.findViewById<ImageView>(R.id.stepperImageOneId)
        imgStepperTwo = view.findViewById<ImageView>(R.id.stepperImageTwoId)
        imgStepperThree = view.findViewById<ImageView>(R.id.stepperImageThreeId)
        imgStepperFour = view.findViewById<ImageView>(R.id.stepperImageFourId)
        imgInformation = view.findViewById<ImageView>(R.id.imgInformationId)

        txtStepperOne = view.findViewById<TextView>(R.id.txtStepperOneId)
        txtStepperTwo = view.findViewById<TextView>(R.id.txtStepperTwoId)
        txtStepperThree = view.findViewById<TextView>(R.id.txtStepperThreeId)
        txtStepperFour = view.findViewById<TextView>(R.id.txtStepperFourId)
        textStep = view.findViewById<TextView>(R.id.textStepId)

        imgStepperOne?.setImageResource(R.drawable.check_circle_outline)
        imgStepperTwo?.setImageResource(R.drawable.check_circle_outline)
        imgStepperThree?.setImageResource(R.drawable.checkbox_blank_circle)

        txtStepperThree?.setTypeface(null, Typeface.BOLD)
        textStep?.text = "Step 3 : Building information and other insurance information"


        imgInformation?.setOnClickListener {

            InformationDialog().customDialog(requireActivity(), resources.getString(R.string.buildinginfo))

        }


        val addButton = view.findViewById<ImageButton>(R.id.img_btn_add_person)
        val linearLayout = view.findViewById<LinearLayout>(R.id.linearLayout)
        val continueButton = view.findViewById<Button>(R.id.continueButton)
        continueButton.setOnClickListener { view ->
            listener?.onBuildingInfo(view)
        }

        var switchList: Bundle
        var buildingTextList: Bundle
        var viewId: Int = 0
        addButton.setOnClickListener { view ->

            val buildingLayout = inflater.inflate(R.layout.fragment_add_building_item, container, false)
            val switch_rented = buildingLayout.findViewById<Switch>(R.id.switch_rented)
            val lbl_insurance_amt_solved = buildingLayout.findViewById<TextInputEditText>(R.id.lbl_insurance_amt_solved)

            ++viewId
            switch_rented.id = viewId
            lbl_insurance_amt_solved.id = viewId

            linearLayout.addView(buildingLayout)

            //TODO get switch event listner value
            //switchList = bundleOf(viewId.toString() to lbl_insurance_amt_solved.text.toString())

            buildingTextList = bundleOf(viewId.toString() to lbl_insurance_amt_solved.text.toString())
        }


        return view
    }

    fun onButtonPressed(view: View) {
        listener?.onBuildingInfo(view)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnBuildingInfoListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnBuildingInfoListener {
        fun onBuildingInfo(view: View)
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar!!.title = resources.getString(R.string.building_info)
    }
}